## Mastermind
A simple implementation of the classic game Mastermind.

The game is console based. You start by entering a code consisting of 4 colors represented by 'R' for Red, 'G' for Green, 'B' for blue, 'Y' for Yellow, 'O' for Orange and 'W' for White. Any other input (including small case letters) will be regarded as invalid. The same color can be used more than once.

Next you (or an oppossing player) must attempt to guess the code. The input accepted is again 'R' for Red, 'G' for Green, 'B' for blue, 'Y' for Yellow, 'O' for Orange and 'W' for White. After each guess the user is provided feedback for the guess, 'B' for a correct placed correctly colored pin, 'W' for an incorrect placed correctly colored pin. An incorrect color is not given feedback. Note that the position of feedback pins does not necessarily correspond to the color pins.

Enjoy! :)
