CXX=g++
#CPPFLAGS=-g $(shell root-config --cflags)
#LDFLAGS=-g $(shell root-config --ldflags)
#LDLIBS=$(shell root-config --libs)

DEPS := $(shell find src/ -name '*.h')
SRCS := $(shell find src/ -name '*.cpp')
OBJS := $(SRCS:%=%.o)

# Create main execuatable
mastermind: $(OBJS)
	$(CXX) $(LDFLAGS) -o mastermind $(OBJS) $(LDLIBS) 

# Build objects
src/%.cpp.o: src/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CFLAGS) -I./src/

.PHONY: clean

# Clean up
clean:
	rm -f src/*.o tests/*.o mastermind testrunner

# Sources and objects used when running tests.
SRCS_WITHOUT_MAIN := $(shell find src/ -name '*.cpp' | grep -v main)
OBJS_WITHOUT_MAIN := $(SRCS_WITHOUT_MAIN:%=%.o)
TEST_SRCS := $(shell find tests/ -name '*.cpp')
TEST_OBJS := $(TEST_SRCS:%=%.o)

# Setup of GTest
GTEST_DIR = /usr/src/gtest/
GTEST_HEADERS = /usr/include/gtest/*.h \
                /usr/include/gtest/internal/*.h
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

# Special GTest runner
gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c $(GTEST_DIR)/src/gtest-all.cc -o tests/gtest-all.o

# Special GTest main
gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c $(GTEST_DIR)/src/gtest_main.cc -o tests/gtest_main.o

# Build test objects
tests/%.cpp.o: tests/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CFLAGS) -I./src/

# Create testrunner
tests: $(OBJS_WITHOUT_MAIN) $(TEST_OBJS) gtest-all.o gtest_main.o
	$(CXX) $(LDFLAGS) -o testrunner $(OBJS_WITHOUT_MAIN) $(TEST_OBJS) tests/gtest-all.o tests/gtest_main.o $(LDLIBS) -lpthread

# Run tests
runtests: tests
	./testrunner
