# Technical Documentation

This document will give a brief overview of the implementation.

## State diagram
See below for a state diagram of how the game works. This does not reflect extactly how the implementation works, but it provides a good overview of the flow of the game.

![State diagram](http://www.plantuml.com/plantuml/proxy?fmt=svg&src=https://gitlab.com/tgc-dk/mastermind/raw/master/docs/state-diagram.puml)

## Class diagrams
See below for a class diagram that shows how the classes are related.

![Class diagram](http://www.plantuml.com/plantuml/proxy?fmt=svg&src=https://gitlab.com/tgc-dk/mastermind/raw/master/docs/class-diagram.puml)

See below for a class diagrams that shows how classes are used.

![Class diagram](http://www.plantuml.com/plantuml/proxy?fmt=svg&src=https://gitlab.com/tgc-dk/mastermind/raw/master/docs/class-diagram2.puml)

## Implementation Details
The game is driven by the Game class, which handles the user input and controls the flow of the game, as visualized in the state diagram above.

First a code (of type CodeToBreak) is requsted from a player and placed in the member variable codeToBreak. Next the the main loop runs as standard 8 times, giving the player 8 guesses.

## TODO
* Make the feedback position random.