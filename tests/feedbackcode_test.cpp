#include <gtest/gtest.h>
#include "feedbackcode.h"

using namespace std;

// Adding pins should be possible 4 times and fail the 5th.
TEST(FeedbackCodeTest, AddPin) {
    FeedbackCode code;
    FeedbackPin* pin0 = new FeedbackPin();
    pin0->setColor('B');
    EXPECT_TRUE(code.addPin(pin0));
    FeedbackPin* pin1 = new FeedbackPin();
    pin1->setColor('W');
    EXPECT_TRUE(code.addPin(pin1));
    FeedbackPin* pin2 = new FeedbackPin();
    pin2->setColor('B');
    EXPECT_TRUE(code.addPin(pin2));
    FeedbackPin* pin3 = new FeedbackPin();
    pin3->setColor('W');
    EXPECT_TRUE(code.addPin(pin3));
    FeedbackPin* pin4 = new FeedbackPin();
    pin4->setColor('B');
    EXPECT_FALSE(code.addPin(pin4));
}


// Adding pins and extracting the code should be possible
TEST(FeedbackCodeTest, AddAndGetCode) {
    FeedbackCode code;
    FeedbackPin* pin0 = new FeedbackPin();
    pin0->setColor('B');
    EXPECT_TRUE(code.addPin(pin0));
    FeedbackPin* pin1 = new FeedbackPin();
    pin1->setColor('W');
    EXPECT_TRUE(code.addPin(pin1));
    FeedbackPin* pin2 = new FeedbackPin();
    pin2->setColor('B');
    EXPECT_TRUE(code.addPin(pin2));
    FeedbackPin* pin3 = new FeedbackPin();
    pin3->setColor('W');
    EXPECT_TRUE(code.addPin(pin3));
    EXPECT_EQ(string("BWBW"), code.getCode());
}
