#include <gtest/gtest.h>
#include "codetobreak.h"

using namespace std;

// Setting code to 'RGBT' should fail since 'T' is not valid
TEST(CodeToBreakTest, InvalidColor) {
    CodeToBreak code;
    std::string codeInput("RGBT");
    EXPECT_FALSE(code.setCode(codeInput));
}

// Setting code to 'RGBY' should succeed since all colors are valid
TEST(CodeToBreakTest, ValidColor) {
    CodeToBreak code;
    std::string codeInput("RGBY");
    EXPECT_TRUE(code.setCode(codeInput));
}

// Setting code to 'RGB' should faild since the code is too short
TEST(CodeToBreakTest, CodeToShort) {
    CodeToBreak code;
    std::string codeInput("RGB");
    EXPECT_FALSE(code.setCode(codeInput));
}

// Setting code to 'RGBY' should faild since the code is too long
TEST(CodeToBreakTest, CodeToLong) {
    CodeToBreak code;
    std::string codeInput("RGBYO");
    EXPECT_FALSE(code.setCode(codeInput));
}

// Compare codes - correct guess
TEST(CodeToBreakTest, CompareCodesBBBB) {
    CodeToBreak code0;
    std::string codeInput0("RGBY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("RGBY");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("BBBB"), feedback->getCode());
    delete feedback;
}

// Compare codes - 2 rigths, 2 wrong
TEST(CodeToBreakTest, CompareCodesBB) {
    CodeToBreak code0;
    std::string codeInput0("RGBY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("OGBW");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("BB"), feedback->getCode());
    delete feedback;
}

// Compare codes - 2 rigths, 1 wrong, 1 partially right
TEST(CodeToBreakTest, CompareCodesBBW) {
    CodeToBreak code0;
    std::string codeInput0("RGBY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("OGBR");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("BBW"), feedback->getCode());
    delete feedback;
}

// Compare codes - 2 wrong, 2 partially right
TEST(CodeToBreakTest, CompareCodesWW) {
    CodeToBreak code0;
    std::string codeInput0("RGBY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("BRWO");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("WW"), feedback->getCode());
    delete feedback;
}

// Compare codes - 1 right, 1 partially right, multiple occurences
TEST(CodeToBreakTest, CompareCodesBWM1) {
    CodeToBreak code0;
    std::string codeInput0("RGBY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("ROYR");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("BW"), feedback->getCode());
    delete feedback;
}

// Compare codes - 1 right, 1 partially right, multiple occurences
TEST(CodeToBreakTest, CompareCodesBWM2) {
    CodeToBreak code0;
    std::string codeInput0("RGRY");
    EXPECT_TRUE(code0.setCode(codeInput0));

    CodeToBreak code1;
    std::string codeInput1("ORRR");
    EXPECT_TRUE(code1.setCode(codeInput1));
    
    FeedbackCode* feedback = code0.compare(&code1);
    EXPECT_EQ(string("BW"), feedback->getCode());
    delete feedback;
}
