#include <gtest/gtest.h>
#include "feedbackpin.h"

// Setting FeedbackPin color to 'T' should fail since it is not a feedback code
TEST(FeedbackPinTest, InvalidColor) {
    FeedbackPin* pin = new FeedbackPin();
    EXPECT_FALSE(pin->setColor('T'));
    delete pin;
}

// Setting FeedbackPin color to 'B' (Black) should succeed
TEST(FeedbackPinTest, ValidColor) {
    FeedbackPin* pin = new FeedbackPin();
    EXPECT_TRUE(pin->setColor('B'));
    delete pin;
}
