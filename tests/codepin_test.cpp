#include <gtest/gtest.h>
#include "codepin.h"

// Setting CodePin color to 'T' should fail since it is not a color code
TEST(CodePinTest, InvalidColor) {
    CodePin* pin = new CodePin();
    EXPECT_FALSE(pin->setColor('T'));
    delete pin;
}

// Setting CodePin color to 'G' (Green) should succeed
TEST(CodePinTest, ValidColor) {
    CodePin* pin = new CodePin();
    EXPECT_TRUE(pin->setColor('G'));
    delete pin;
}
