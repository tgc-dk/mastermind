#include "feedbackpin.h"

bool FeedbackPin::validateColor(char color) {
    // Check that the color is legal for this pin type
    std::string allowedValues("BW");
    if (allowedValues.find(color) == std::string::npos) {
        return false;
    }
    return true;
}

