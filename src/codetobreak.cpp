#include "codetobreak.h"
#include "feedbackcode.h"
#include <iostream>
#include <string>

using namespace std;

CodeToBreak::CodeToBreak() {
}

bool CodeToBreak::setCode(string &code) {
    // Validate and set code
    // First validate size, must be 4
    if (code.length() != 4) {
        cout << "Invalid code length" << endl;
        return false;
    }
    int idx = 0;
    for (string::iterator it=code.begin(); it != code.end(); ++it) {
        // Validate that the character is valid
        if (!pins[idx].setColor(*it)) {
            return false;
        }
        ++idx;
    }
    return true;
}

const CodePin& CodeToBreak::getPinAt(int idx) {
    return this->pins[idx];
}

FeedbackCode* CodeToBreak::compare(CodeToBreak* other) {
    FeedbackCode* ret = new FeedbackCode();
    int idx = 0;
    // Mark guess pins as used or not
    bool usedGuess[4];
    // Mark code pins if they have been guessed or not
    bool codeGuessed[4];
    // First check for "direct hits" (black pins)
    for (int i = 0; i < 4; ++i) {
        if (other->getPinAt(i) == this->pins[i]) {
            FeedbackPin* pin = new FeedbackPin();
            pin->setColor('B');
            ret->addPin(pin);
            usedGuess[i] = true;
            codeGuessed[i] = true;
        } else {
            usedGuess[i] = false;
            codeGuessed[i] = false;
        }
    }
    // Now check for "indirect hits" (white pins)
    for (int i = 0; i < 4; ++i) {
        // Skip pins already guessed
        if (usedGuess[i]) {
            continue;
        }
        // Run through the code to look for indirect hits
        for (int j = 0; j < 4; ++j) {
            // Skip pins already guessed
            if (codeGuessed[j]) {
                continue;
            }
            if (other->getPinAt(i) == this->pins[j]) {
                FeedbackPin* pin = new FeedbackPin();
                pin->setColor('W');
                ret->addPin(pin);
                usedGuess[i] = true;
                codeGuessed[j] = true;
                break;
            }
        }
    }
    return ret;
}

string CodeToBreak::getCode() {
    string code;
    for (int i = 0; i < 4; ++i) {
        code.append(1, this->pins[i].getColor());
    }
    return code;
}
