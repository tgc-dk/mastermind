#pragma once

#include "code.h"
#include "feedbackpin.h"
#include <vector>

class FeedbackCode : public Code {
    public:
        FeedbackCode();
        ~FeedbackCode();
        bool addPin(FeedbackPin* pin);
        FeedbackPin* getPinAt(int idx);
        std::string getCode();
    private:
        std::vector<FeedbackPin*> pins;
};
