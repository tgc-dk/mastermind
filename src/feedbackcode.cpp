#include "feedbackcode.h"
#include <vector>

using namespace std;

FeedbackCode::FeedbackCode() {
}

FeedbackCode::~FeedbackCode() {
    for (vector<FeedbackPin*>::iterator it=this->pins.begin(); it != this->pins.end(); ++it) {
        delete (*it);
    }
}

bool FeedbackCode::addPin(FeedbackPin* pin) {
    if (pins.size() == 4) {
        // There should only be 4 feedback pins, not more!
        return false;
    }
    pins.push_back(pin);
    return true;
}

FeedbackPin* FeedbackCode::getPinAt(int idx) {
    return this->pins[idx];
}

string FeedbackCode::getCode() {
    string code;
    for (vector<FeedbackPin*>::iterator it=this->pins.begin(); it != this->pins.end(); ++it) {
        code.append(1, (*it)->getColor());
    }
    return code;
}
