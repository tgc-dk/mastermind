#pragma once

#include <string>

class Code {
    virtual std::string getCode() = 0;
};
