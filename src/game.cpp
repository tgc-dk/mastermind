#include "game.h"

#include <iostream>
#include <string>

using namespace std;

Game::Game(int attempts)
    : attempts(attempts) {
        this->codeToBreak = new CodeToBreak();
        this->currentGuess = new CodeToBreak();
}

Game::~Game() {
    if (this->codeToBreak) {
        delete this->codeToBreak;
    }
    if (this->currentGuess) {
        delete this->currentGuess;
    }
}

bool Game::setStartCode() {
    cout << "Enter the 4 character code that must be guessed." << endl;
    string code = this->getCodeFromPlayer();
    int inputTries = 0;
    bool inputValid = this->codeToBreak->setCode(code);
    while(!inputValid && inputTries < 3) {
        cout << "Invalid code input, please try again!" << endl;
        code = this->getCodeFromPlayer();
        inputValid = this->codeToBreak->setCode(code);
        ++inputTries;
    }
    if (!inputValid) {
        cout << "Too many failed attempts to enter a valid code, quiting!" << endl;
        return false;
    }
    return true;
}

bool Game::getGuessCode() {
    cout << "Enter the 4 character code you think is the code." << endl;
    string code = this->getCodeFromPlayer();
    int inputTries = 0;
    bool inputValid = this->currentGuess->setCode(code);
    while(!inputValid && inputTries < 3) {
        cout << "Invalid code input, please try again!" << endl;
        code = this->getCodeFromPlayer();
        inputValid = this->currentGuess->setCode(code);
        ++inputTries;
    }
    if (!inputValid) {
        cout << "Too many failed attempts to enter a valid code, quiting!" << endl;
        return false;
    }
    return true;
}

string Game::getCodeFromPlayer() {
    string code;
    cout << "Valid input is 'R' for Red, 'G' for Green, 'B' for blue, 'Y' for Yellow, 'O' for Orange, 'W' for White: ";
    getline (cin, code);
    return code;
}

string Game::processGuess() {
    FeedbackCode* feedback = this->codeToBreak->compare(this->currentGuess);
    string ret = feedback->getCode();
    delete feedback;
    return ret;
}

void Game::runGame() {
    cout << "Welcome to MasterMind!" << endl << endl;
    if (!this->setStartCode()) {
        // Getting valid start code failed!
        return;
    }
    cout << "\x1B[2J\x1B[H" << "Lets start the game!!" << endl << endl;
    for (int i = 0; i < this->attempts; ++i) {
        cout << "You have "  << this->attempts - i << " remaining guesses." << endl;
        if (!this->getGuessCode()) {
            cout << "Failed to provide a valid guess! Ending the game!" << endl;
            return;
        }
        string feedback = this->processGuess();
        if (feedback.compare("BBBB") == 0) {
            cout << "Congratulations! You won! The guess was completely correct! The game has ended." << endl;
            return;
        }
        cout << "The guess was not correct, the feedback for you is: " << feedback << endl << endl;
    }
    cout << "You did not succeed in breaking the code! You Lost! The code you were looking for was: " << this->codeToBreak->getCode() << " The game has ended." << endl;
}
