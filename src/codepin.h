#pragma once

#include "pin.h"

class CodePin : public Pin {
    protected:
        bool validateColor(char color);

};
