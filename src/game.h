#pragma once

#include "codetobreak.h"

class Game {

    public:
        Game(int attempts);
        ~Game();
        void runGame();

    private:
        bool setStartCode();
        bool getGuessCode();
        std::string getCodeFromPlayer();
        std::string processGuess();

        CodeToBreak* codeToBreak;
        CodeToBreak* currentGuess;
        int attempts;
};
