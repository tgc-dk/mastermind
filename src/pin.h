#pragma once

#include <string>

class Pin {
    public:
        bool setColor(char color);
        char getColor();
        bool operator==(const Pin& other) const;
    protected:
        virtual bool validateColor(char color) = 0;
        char pinColor;


};
