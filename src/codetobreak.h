#pragma once

#include "code.h"
#include "codepin.h"
#include "feedbackcode.h"

class CodeToBreak : public Code {
    public:
        CodeToBreak();
        bool setCode(std::string &code);
        const CodePin& getPinAt(int idx);
        FeedbackCode* compare(CodeToBreak* other);
        std::string getCode();
    private:
        CodePin pins[4];
};
