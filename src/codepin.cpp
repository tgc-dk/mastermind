#include "codepin.h"
#include <iostream>
#include <string>

using namespace std;

bool CodePin::validateColor(char color) {
    // Check that the color is legal for this pin type
    string allowedValues("RGBYOW");
    if (allowedValues.find(color) == string::npos) {
        return false;
    }
    return true;
}
