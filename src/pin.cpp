#include "pin.h"

bool Pin::setColor(char color) {
    // Check that the color is legal for this pin type
    if (!this->validateColor(color)) {
        return false;
    }
    this->pinColor = color;
    return true;
}

char Pin::getColor() {
    return this->pinColor;
}

bool Pin::operator==(const Pin& other) const {
    return other.pinColor == this->pinColor;
}

