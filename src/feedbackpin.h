#pragma once

#include "pin.h"

class FeedbackPin : public Pin {
    protected:
        bool validateColor(char color);

};
